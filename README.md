Laravel Instiller Connection
============

[![Laravel](https://img.shields.io/badge/Laravel-5.5-orange.svg?style=flat-square)](http://laravel.com)

Integration to DPP Instiller Stats API

### Installation

Install with composer


```
"repositories": [
        {
            "type": "vcs",
            "url": "https://git@bitbucket.org/teamdpp/instiller.git"
        }
]
```


```composer require teamdpp/instiller```



Optionally setup the [config/instiller.php](config/instiller.php) file

    php artisan vendor:publish

### Usage

Add the below format to the .env

```
INSTILLER_SYNC = 0
INSTILLER_API_ID = ""
INSTILLER_API_KEY = ""
INSTILLER_CAMPAIGN_ID = 3888,3950
```

Manual Run

```
php artisan instiller:process
```

If the cron is running it will auto run every 3 hours