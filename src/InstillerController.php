<?php

namespace teamdpp\instiller;

use App\Models\Prospect;
use Carbon\Carbon;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class InstillerController extends Controller
{
    function __construct() {

        $this->api_id = env('INSTILLER_API_ID');
        $this->api_key = env('INSTILLER_API_KEY');
        $this->campaign_id = env('INSTILLER_CAMPAIGN_ID');

        $directory='instiller';
        if (!Storage::has($directory)) {
            $resp= Storage::makeDirectory($directory);
        }

    }

    function buildUrl($path, $args) {
        $query =  http_build_query($args);
        return 'https://stats.edpp.co.uk/rest/'.$path.'?api_id='.$this->api_id.'&api_key='.$this->api_key.'&'.$query;
    }


    function fetchCampaignEngagement() {

        $campaignIds = explode(',', $this->campaign_id);
        foreach ($campaignIds as $campaignId) {
            echo $campaignId."\n";
            $this->importCampaignEngagement($campaignId, 'opens');
            $this->importCampaignEngagement($campaignId, 'clicks');

        }

    }


    function importCampaignEngagement($campaignId, $type) {

        $url = $this->buildUrl('campaigns/engagement', ['campaign_id' => $campaignId, 'date_from' => Carbon::now()->subDays(24)->toDateTimeString(), 'date_to' => Carbon::now()->toDateTimeString(), 'type' => $type]);

        //$client = new \GuzzleHttp\Client();

        $client = new Client();

        $path = storage_path('app/instiller').'/engagement.zip';
        $csvPath = storage_path('app/instiller').'/engagement';
        $download = $client->request('GET', $url, [
            'sink' => $path,
        ]);

        $zipper = new Zipper();
        $zipper->make($path)->extractTo($csvPath);


        $csvFiles = glob($csvPath.'/*.csv');

        foreach ($csvFiles as $csvFile) {
            $csv = \Excel::load($csvFile, function($reader) { })->get();
            foreach ($csv as $data) {
                try {
                    $prospect = Prospect::where('email', $data['email_address'])->first();

                    if ($type == 'opens') {
                        if ($data['open_count'] > 0) {
                            if ($prospect) {
                                $prospect->email_opened = 1;
                                $prospect->save();
                            }
                        }
                    } elseif ($type == 'clicks') {
                        if ($data['click_count'] > 0) {
                            if ($prospect) {
                                $prospect->email_clicked = 1;
                                $prospect->save();
                            }
                        }
                    }
                } catch (\Exception $e) {

                }
            }
            unlink($csvFile);
        }
        unlink($path);
        rmdir($csvPath);
    }
}
