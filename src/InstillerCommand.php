<?php
namespace teamdpp\instiller\Commands;

use Illuminate\Console\Command;
use teamdpp\instiller\InstillerController;

class InstillerCommand extends Command {

    protected $signature = 'instiller:process';

    protected $description = 'Process Instiller';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        if (env('INSTILLER_SYNC')) {
            $InstillerController = new InstillerController();
            echo $InstillerController->fetchCampaignEngagement();
        }
    }

}
