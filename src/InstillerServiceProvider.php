<?php
namespace teamdpp\instiller;

//require __DIR__ .'/../vendor/autoload.php';

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class InstillerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';

        $this->publishes([
            __DIR__.'/config/instiller.php' => config_path('instiller.php'),
        ], 'config');


        $this->commands([
            \teamdpp\instiller\Commands\InstillerCommand ::class,
        ]);

        $this->app->booted(function () {
            $schedule = app(Schedule::class);
            $schedule->command('instiller:process')->cron('0 */3 * * *');
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('teamdpp\instiller\InstillerController');

        $this->mergeConfigFrom( __DIR__.'/config/instiller.php', 'instiller');

    }
}
